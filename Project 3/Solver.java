import java.util.*;

public class Solver {
	
	public static boolean debug, debugMsg, debugOK, dPossibleMoves;
	private HashSet<HashSet<Piece>> checked;
	private Board myBoard;
	private HashSet<Piece> goalConfig;
    private Stack<Move> moveStack;
    private ArrayList<Move> path;
    private int countMoves;
	
	public Solver(String options, String initialFile, String goalFile){
		debug = debugMsg = debugOK = false;
		if (!options.equals("")){
			debug = true;
			if (options.equals("-omessage")){
				debugMsg = true;
			} else if(options.equals("-ook")){
				debugOK = true;
			} else if (options.equals("-ooptions")){
				System.out.println("Debugging Options:");
				System.out.println("-omessage: Prints messages that give information about the status");
				System.out.println("           of program execution. (i.e. when a method is called or");
				System.out.println("           the board configuration after a piece is moved)");
				System.out.println("-ook: isOK is called after any piece on the board is moved.");
				System.out.println("-ooptions: Lists all debugging options");
				System.exit(0);
			} else {
				System.err.println("Invalid option, valid options are -omessage and -ooptions");
				System.exit(1);
			}
		}
		createInitialBoard(new InputSource(initialFile));
		createGoalConfig(new InputSource(goalFile));
		countMoves = 0;
	}
	
	// Creates a Board object that represents the initial
	// configuration of the board that is specified by the input file
	private void createInitialBoard(InputSource input){
		// read the number of rows and columns of the board to be constructed
		String line = input.readLine();
		int space = line.indexOf(' ');
		int rows = Integer.parseInt(line.substring(0,space));
		int columns = Integer.parseInt(line.substring(space+1));
		//ArrayList<Piece> pieces = new ArrayList<Piece>();
		HashSet<Piece> pieces = new HashSet<Piece>( );
		
		// read the positions of each piece, construct the piece,
		// and add the piece to the ArrayList pieces
		line = input.readLine();
		while (line != null){
			pieces.add(new Piece(line));
			line = input.readLine();
		}
		
		// construct the board
		myBoard = new Board(pieces, rows, columns);
	}
	
	// Creates a HashSet that contains the Piece objects
	// that are in the goal configuration
	private void createGoalConfig(InputSource input){
		goalConfig = new HashSet<Piece>();
		String line = input.readLine();
		while (line != null){
			goalConfig.add(new Piece(line));
			line = input.readLine();
		}
	}
		
	public static void main(String[] args){
		String option, initialFile, goalFile; // variables to store arguments to Solver
		option = initialFile = goalFile = "";
		
		// store the arguments to Solver in the appropriate variables
		// print an error message and exit if 2 or 3 arguments were
		// not given
		if (args.length == 2){
			initialFile = args[0];
			goalFile = args[1];
		} else if (args.length == 3){
			option = args[0];
			initialFile = args[1];
			goalFile = args[2];
		} else {
			System.err.println("Expected 2 or 3 arguments");
			System.exit(1);
		}
		
		Solver solver = new Solver(option, initialFile, goalFile);
		solver.solveDepthIterative();
	}
	
	// An earlier version of the solve method that was not
	// efficient enough. Included for comparison purposes (see readme)
	public void solveRecursive(){
		String output;
		try {
			output = solveRecursiveHelper(myBoard.getPieces());
		} catch (IllegalMoveException ex){
			throw new IllegalStateException("Solver attempted illegal move:\n"+ex.getMessage());
		}
		if(!output.isEmpty())
			System.out.println(output);
	}
	
	// A helper for an earlier version of the solve method that was not
	// efficient enough. Included for comparison purposes (see readme)
	private String solveRecursiveHelper(HashSet<Piece> config) throws IllegalMoveException{
		ArrayList<Move> moves = myBoard.getPossibleMoves();
		if(debugMsg){
			System.out.println("Possible Moves:");
			ListIterator<Move> m = moves.listIterator();
			while (m.hasNext())
				System.out.println(m.next());
		}
		ListIterator<Move> movesIter = moves.listIterator();
		String result = "";
		
		while (movesIter.hasNext()){
			Move m = movesIter.next();
			Piece oldPiece = m.piece;
			Piece newPiece = new Piece(oldPiece, m.direction);
			if(debugMsg){
				System.out.println("Checking Move "+m+" for goal");
			}

			config.remove(oldPiece);
			config.add(newPiece);
			
			if (!checked.contains(config)){
				checked.add(copyConfig(config));
				if(isSolved(config)){
					result = oldPiece.getPosition().x+" "+oldPiece.getPosition().y+" "+
							 newPiece.getPosition().x+" "+newPiece.getPosition().y;
					return result;
				}
			} else {
				movesIter.remove();
			}

			config.remove(newPiece);
			config.add(oldPiece);
		}
		
		
		for (Move m : moves){
			Piece oldPiece = new Piece(m.piece); //important to make a copy
			Piece newPiece = new Piece(oldPiece, m.direction);
			myBoard.move(m.piece, m.direction);
			config.remove(oldPiece);
			config.add(newPiece);
			String newConfig  = solveRecursiveHelper(copyConfig(config));
			if (!newConfig.isEmpty()){
				result += oldPiece.getPosition().x+" "+oldPiece.getPosition().y+" "+
						 newPiece.getPosition().x+" "+newPiece.getPosition().y+"\n"+newConfig;
				return result;
			}
			else {
				myBoard.undo(m.piece, m.direction);
				config.remove(newPiece);
				config.add(oldPiece);
			}
		}
		return result;
	}

	// Returns a copy of the original HashSet
	// The copy has Pieces that are new Pieces identical to
	// those in original
	private HashSet<Piece> copyConfig(HashSet<Piece> original){
		HashSet<Piece> copy = new HashSet<Piece>();
		for (Piece p : original){
			copy.add(new Piece(p.toString()));
		}
		return copy;
	}
	
    // return True if this Board match the goal file
    private boolean isSolved(HashSet<Piece> currentConfig){
        for (Piece p : goalConfig){
                //System.out.println(p);
                if (!currentConfig.contains(p)){
                        return false;
                }
        }
        return true;
    }	
	
    // Uses a stack to perform a depth-first traversal of all possible moves.
    // To go back up the tree, moves that are flagged as undo are placed on
    // the stack. When a move is popped off the stack, the move to undo it is
    // pushed onto the stack. Once all the descendants of the move have been
    // checked, that move will then be undone, and its sibling moves can be
    // performed.
    public void solveDepthIterative(){
    	checked = new HashSet<HashSet<Piece>>();  // HashSet of all configurations that have already been checked
    	moveStack = new Stack<Move>(); // Moves that need to be performed to check new configurations
    	path = new ArrayList<Move>(); // List of moves to get to solution
    	
		if(debugMsg) {
			System.out.println("Goal Configuration: ");
			System.out.println(goalConfig);
			System.out.println();
		}
    	
    	HashSet<Piece>config = copyConfig(myBoard.getPieces( ));
    	//HashSet<Piece> config = myBoard.getPieces();
    	if (!isSolved(config)){
    		/* 
    		 * If the board doesn't start out solved, add the initial configuration
    		 * to checked, and add all possible moves from the initial configuration
    		 * to moveStack.
    		 */
    		checked.add(config);
    		ArrayList<Move> moves = myBoard.getPossibleMoves( );
    		for (Move m : moves){
    			moveStack.push(new Move(false, m.piece, m.direction));
    		}
    		
    		// As long as the moveStack is not empty, there are new configurations to check
    		while (!moveStack.isEmpty()){
    			Move m = moveStack.pop();
    			Piece mPiece = m.piece;
    			Board.Dir mDir = m.direction;
    			// If the move at the top of moveStack is flagged as undo, undo that move
    			// and remove it from path.
    			// Otherwise add the move to the path, make the move on the board, and
    			// push a move to the stack that will undo the current move.
    			try{
    				if (m.undo){
    					myBoard.undo(mPiece, mDir);
    					path.remove(path.size()-1);
    					continue;
    				} else {
    		    		if(debugMsg){
    		    			System.out.println("Possible Moves:");
    		    			ListIterator<Move> m2 = moves.listIterator();
    		    			while (m2.hasNext())
    		    				System.out.println(m2.next());
    		    		}
    					if(debugMsg){
    						System.out.println("Current Move: "+m);
    					}
    					path.add(new Move(new Piece(mPiece), mDir));
	    				myBoard.move(mPiece, mDir);
	    				moveStack.push(new Move(true, mPiece, mDir));
	    				countMoves++;
	    		    	if(debugMsg){
	    		    		System.out.println("Number of Moves Made So Far: " + countMoves);
	    		    	}
    				}
    			} catch (IllegalMoveException ex){
    				throw new IllegalStateException(ex.getMessage());
    			}
    			// Check if the current configuration of the board has already been checked.
    			// If not, check if it is the solution. If not, add it to checked and push
    			// all possible moves from this point onto moveStack.
    			config = copyConfig(myBoard.getPieces( ));
		    	if(debugMsg){
		    		System.out.println("Board After Move: ");
		            System.out.println(config);
		            System.out.println();
		    	}
    			if (checked.contains(config))
    				continue;
    			if (isSolved(config)){
    				break;
    			} else {
    				checked.add(config);
    				for (Move mov : myBoard.getPossibleMoves()){
    	    			moveStack.push(new Move(false, mov.piece, mov.direction));
    	    		}
    			}
    		}
    	}
    	//Print out the moves that lead to the solution
    	for (Move m: path)
    		System.out.println(m.output());
    }

}
