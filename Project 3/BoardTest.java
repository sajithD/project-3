import static org.junit.Assert.*;

import java.awt.Point;
import java.util.*;

import org.junit.Test;
//

public class BoardTest {
	
	@Test
	public void testBoardConstructor(){
		Piece p;
		HashSet<Piece> pieces;
		Board board;
		// empty 1x1 board
		pieces = new HashSet<Piece>();
		board = new Board(pieces, 1, 1);
		assertNull(board.getPiece(0, 0));
		try {
			board.getPiece(0, 1);
			fail();
		} catch (IllegalArgumentException ex){
		}
		try {
			board.getPiece(1, 0);
			fail();
		} catch (IllegalArgumentException ex){
		}
		
		// 1x1 board with piece
		p = new Piece("0 0 0 0");
		pieces = new HashSet<Piece>();
		pieces.add(p);
		board = new Board(pieces, 1, 1);
		assertEquals(p, board.getPiece(0, 0));
		try {
			board.getPiece(0, 1);
			fail();
		} catch (IllegalArgumentException ex){
		}
		try {
			board.getPiece(1, 0);
			fail();
		} catch (IllegalArgumentException ex){
		}
		
		// 10x9 board with pieces and empty spaces
		pieces = new HashSet<Piece>();
		Piece p1 = new Piece("5 5 6 6"); //piece in center
		Piece p2 = new Piece("8 7 9 8"); // piece in bottom right corner
		Piece p3 = new Piece("5 0 9 0"); // piece along left edge
		Piece p4 = new Piece("3 1 7 3"); // piece touching another piece
		pieces.add(p1);
		pieces.add(p2);
		pieces.add(p3);
		pieces.add(p4);
		board = new Board(pieces, 10, 9);
		
		for (int row=0; row<10; row++){
			for (int col=0; col<9; col++){
				if (row>=5 && row<=6 && col>=5 && col<=6)
					assertEquals(p1, board.getPiece(row, col));
				else if (row>=8 && row<=9 && col>=7 && col<=8)
					assertEquals(p2, board.getPiece(row, col));
				else if (row>=5 && row<=9 && col==0)
					assertEquals(p3, board.getPiece(row, col));
				else if (row>=3 && row<=7 && col>=1 && col<=3)
					assertEquals(p4, board.getPiece(row, col));
				else
					assertNull(board.getPiece(row, col));
			}
		}
		
		// Invalid Board: overlapping pieces
		pieces = new HashSet<Piece>();
		pieces.add(new Piece("0 0 1 1"));
		pieces.add(new Piece("1 1 2 2"));
		try {
			board = new Board(pieces, 3, 3);
			fail("Board with overlapping pieces created");
		} catch (IllegalArgumentException ex){
		}
		
		// Invalid Board: piece over edge of board
		pieces = new HashSet<Piece>();
		pieces.add(new Piece("0 0 0 4"));
		try {
			board = new Board(pieces, 3, 4);
			fail("Board with oversized piece created");
		} catch (IllegalArgumentException ex){
		}
	}
	
	@Test
	public void testGetPiece(){
		Piece p1 = new Piece("0 0 0 0");
		Piece p2 = new Piece("1 0 1 1");
		HashSet<Piece> pieces = new HashSet<Piece>();
		pieces.add(p1);
		pieces.add(p2);
		Board board = new Board(pieces, 4, 3);
		
		//valid calls to getPiece
		assertEquals(p1, board.getPiece(0,0));
		assertEquals(p2, board.getPiece(1,0));
		assertEquals(p2, board.getPiece(1,1));
		assertNull(board.getPiece(0,1));
		assertNull(board.getPiece(2,2));
		assertNull(board.getPiece(3,2));
		
		//invalid calls to getPiece
		try {
			board.getPiece(-1, 0); // negative row
			fail();
		} catch (IllegalArgumentException ex){
		}
		try {
			board.getPiece(0, -1); // negative column
			fail();
		} catch (IllegalArgumentException ex){
		}
		try {
			board.getPiece(4, 0); // row index too big
			fail();
		} catch (IllegalArgumentException ex){
		}
		try {
			board.getPiece(0, 3); // column index too big 
			fail();
		} catch (IllegalArgumentException ex){
		}
		
	}

	@Test
	public void hashCodeTest() {
		
	}
	
	@Test
	public void equalsTest(){
		HashSet<Piece> pieces1, pieces2, pieces3, pieces4;
		Board b1, b2, b3, b4;
		
		// empty boards
		pieces1 = new HashSet<Piece>();
		pieces2 = new HashSet<Piece>();
		b1 = new Board(pieces1, 1, 1);
		b2 = new Board(pieces2, 1, 1);
		b3 = new Board(pieces1, 1, 2);
		b4 = new Board(pieces1, 2, 1);
		assertTrue(b1.equals(b2));
		assertFalse(b1.equals(b3));
		assertFalse(b1.equals(b4));
		
		// boards with 1 piece
		pieces1 = new HashSet<Piece>();
		pieces2 = new HashSet<Piece>();
		pieces3 = new HashSet<Piece>();
		pieces4 = new HashSet<Piece>();
		pieces1.add(new Piece("2 2 3 3"));
		pieces2.add(new Piece("2 2 3 3"));
		pieces3.add(new Piece("2 2 2 2"));
		pieces4.add(new Piece("3 3 4 4"));
		b1 = new Board(pieces1, 7, 7);
		b2 = new Board(pieces2, 7, 7);
		b3 = new Board(pieces3, 7, 7);
		b4 = new Board(pieces4, 7, 7);
		assertTrue(b1.equals(b2));
		assertFalse(b1.equals(b3));
		assertFalse(b1.equals(b4));
		
		// boards with multiple pieces
		pieces1 = new HashSet<Piece>();
		pieces2 = new HashSet<Piece>();
		pieces3 = new HashSet<Piece>();
		pieces4 = new HashSet<Piece>();
		pieces1.add(new Piece("2 2 3 3"));
		pieces1.add(new Piece("0 0 0 0"));
		pieces2.add(new Piece("2 2 3 3"));
		pieces2.add(new Piece("0 0 0 0"));
		pieces3.add(new Piece("2 2 2 2"));
		pieces3.add(new Piece("0 0 0 0"));
		pieces4.add(new Piece("4 4 5 5"));
		pieces4.add(new Piece("1 1 1 1"));
		b1 = new Board(pieces1, 7, 7);
		b2 = new Board(pieces2, 7, 7);
		b3 = new Board(pieces3, 7, 7);
		b4 = new Board(pieces4, 7, 7);
		assertTrue(b1.equals(b2));
		assertFalse(b1.equals(b3));
		assertFalse(b1.equals(b4));
		
		// boards with same pieces but different sizes
		pieces1 = new HashSet<Piece>();
		pieces2 = new HashSet<Piece>();
		pieces1.add(new Piece("2 2 3 3"));
		pieces1.add(new Piece("0 0 0 0"));
		pieces2.add(new Piece("2 2 3 3"));
		pieces2.add(new Piece("0 0 0 0"));
		b1 = new Board(pieces1, 7, 7);
		b2 = new Board(pieces2, 7, 8);
		assertFalse(b1.equals(b2));
	}
	
	@Test
	public void testCheckMove1( ) {
		// Test a board with four non-adjacent 1x1 Pieces, one on each edge
		// Make sure that each Piece cannot move beyond the edge of the Board
		// and can move in all other directions
		Piece left = new Piece("2 0 2 0");
		Piece right = new Piece("2 3 2 3");
		Piece top = new Piece("0 1 0 1");
		Piece bottom = new Piece("4 1 4 1");
		HashSet<Piece> pieces = new HashSet<Piece>( );
		pieces.add(left);
		pieces.add(right);
		pieces.add(top);
		pieces.add(bottom);
		Board b0 = new Board(pieces, 5, 4);
		assertFalse(b0.checkMove(left, Board.Dir.LEFT));
		assertFalse(b0.checkMove(right, Board.Dir.RIGHT));
		assertFalse(b0.checkMove(top, Board.Dir.UP));
		assertFalse(b0.checkMove(bottom, Board.Dir.DOWN));
		assertTrue(b0.checkMove(left, Board.Dir.RIGHT));
		assertTrue(b0.checkMove(left, Board.Dir.UP));
		assertTrue(b0.checkMove(left, Board.Dir.DOWN));
		assertTrue(b0.checkMove(right, Board.Dir.LEFT));
		assertTrue(b0.checkMove(right, Board.Dir.UP));
		assertTrue(b0.checkMove(right, Board.Dir.DOWN));
		assertTrue(b0.checkMove(top, Board.Dir.DOWN));
		assertTrue(b0.checkMove(top, Board.Dir.RIGHT));
		assertTrue(b0.checkMove(top, Board.Dir.LEFT));
		assertTrue(b0.checkMove(bottom, Board.Dir.UP));
		assertTrue(b0.checkMove(bottom, Board.Dir.RIGHT));
		assertTrue(b0.checkMove(bottom, Board.Dir.LEFT));
	}
	
	@Test
	public void testCheckMove2( ) {
		// Test that a Piece cannot move into a space that is already
		// occupied by a Piece, and that Pieces can move into spaces
		// adjacent to a space occupied by a Piece
		Piece left = new Piece("2 0 2 0");
		Piece right = new Piece("2 3 2 3");
		Piece top = new Piece("0 1 0 1");
		Piece bottom = new Piece("4 1 4 1");
		Piece inTheWay  = new Piece("2 1 2 1");
		HashSet<Piece> pieces = new HashSet<Piece>( );
		pieces.add(left);
		pieces.add(right);
		pieces.add(top);
		pieces.add(bottom);
		pieces.add(inTheWay);
		Board b1 = new Board(pieces, 5, 4);
		assertFalse(b1.checkMove(left, Board.Dir.RIGHT));
		assertTrue(b1.checkMove(top, Board.Dir.DOWN));
		assertTrue(b1.checkMove(bottom, Board.Dir.UP));
		assertTrue(b1.checkMove(right, Board.Dir.LEFT));
	}
	
	@Test
	public void testCheckMove3( ) {
		// Test a board with Pieces of multiple shapes and sizes
		Piece top = new Piece("0 3 1 5");
		Piece bottom = new Piece("8 4 9 5");
		Piece left = new Piece("5 0 9 0");
		Piece right = new Piece("3 5 6 8");
		HashSet<Piece> pieces = new HashSet<Piece>( );
		pieces.add(left);
		pieces.add(right);
		pieces.add(top);
		pieces.add(bottom);
		Board b2 = new Board(pieces, 10, 9);
		assertFalse(b2.checkMove(left, Board.Dir.LEFT));
		assertFalse(b2.checkMove(right, Board.Dir.RIGHT));
		assertFalse(b2.checkMove(top, Board.Dir.UP));
		assertFalse(b2.checkMove(bottom, Board.Dir.DOWN));
		assertTrue(b2.checkMove(left, Board.Dir.RIGHT));
		assertTrue(b2.checkMove(left, Board.Dir.UP));
		assertTrue(b2.checkMove(right, Board.Dir.LEFT));
		assertTrue(b2.checkMove(right, Board.Dir.UP));
		assertTrue(b2.checkMove(right, Board.Dir.DOWN));
		assertTrue(b2.checkMove(top, Board.Dir.DOWN));
		assertTrue(b2.checkMove(top, Board.Dir.RIGHT));
		assertTrue(b2.checkMove(top, Board.Dir.LEFT));
		assertTrue(b2.checkMove(bottom, Board.Dir.UP));
		assertTrue(b2.checkMove(bottom, Board.Dir.RIGHT));
		assertTrue(b2.checkMove(bottom, Board.Dir.LEFT));
	}
	
	@Test
	public void testMove1( ) {
		Piece top = new Piece("0 3 1 5");
		Piece bottom = new Piece("8 4 9 5");
		Piece left = new Piece("5 0 9 0");
		Piece right = new Piece("3 5 6 8");
		HashSet<Piece> pieces = new HashSet<Piece>( );
		pieces.add(left);
		pieces.add(right);
		pieces.add(top);
		pieces.add(bottom);
		Board b2 = new Board(pieces, 10, 9);
		try {
			b2.move(top, Board.Dir.DOWN);
			assertEquals(top.getPosition( ), new Point(1, 3));
			assertNull(b2.getPiece(0, 3));
			assertNull(b2.getPiece(0, 4));
			assertNull(b2.getPiece(0, 5));
			assertEquals(b2.getPiece(1, 3), top);
			assertEquals(b2.getPiece(1, 4), top);
			assertEquals(b2.getPiece(1, 5), top);
			b2.isOK();
		} catch(IllegalMoveException | IllegalStateException ime) {
			System.out.println(ime.getMessage( ));
			fail();
		}
	}
	
	// test for moving 1x1 piece
	@Test
	public void testMoveSmall(){
		int r = 5, c = 5; // number of rows and columns on board
		Piece p = new Piece("2 2 2 2");
		HashSet<Piece> pieces = new HashSet<Piece>();
		HashSet<Piece> piecesCheck;
		pieces.add(p);
		Board b = new Board(pieces, r, c);
		Board bCheck;
		
		try{
			b.move(p, Board.Dir.LEFT); // move left
			piecesCheck = new HashSet<Piece>();
			piecesCheck.add(new Piece("2 1 2 1"));
			bCheck = new Board(piecesCheck, r, c);
			assertEquals(bCheck, b);
			
			b.move(p, Board.Dir.RIGHT); // move right
			piecesCheck = new HashSet<Piece>();
			piecesCheck.add(new Piece("2 2 2 2"));
			bCheck = new Board(piecesCheck, r, c);
			assertEquals(bCheck, b);
			
			b.move(p, Board.Dir.DOWN); // move down
			piecesCheck = new HashSet<Piece>();
			piecesCheck.add(new Piece("3 2 3 2"));
			bCheck = new Board(piecesCheck, r, c);
			assertEquals(bCheck, b);
			
			b.move(p, Board.Dir.UP); // move up
			piecesCheck = new HashSet<Piece>();
			piecesCheck.add(new Piece("2 2 2 2"));
			bCheck = new Board(piecesCheck, r, c);
			assertEquals(bCheck, b);
		} catch (IllegalMoveException ex){
			System.err.println(ex.getMessage());
			fail();
		}
	}
	
	// test for moving a larger piece
	@Test
	public void testMoveLarge(){
		int r = 7, c = 7;
		Piece p = new Piece("2 2 4 3");
		HashSet<Piece> pieces = new HashSet<Piece>();
		HashSet<Piece> piecesCheck;
		pieces.add(p);
		Board b = new Board(pieces, r, c);
		Board bCheck;
		
		try{
			b.move(p, Board.Dir.LEFT); // move left
			piecesCheck = new HashSet<Piece>();
			piecesCheck.add(new Piece("2 1 4 2"));
			bCheck = new Board(piecesCheck, r, c);
			assertEquals(bCheck, b);
			
			b.move(p, Board.Dir.RIGHT); // move right
			piecesCheck = new HashSet<Piece>();
			piecesCheck.add(new Piece("2 2 4 3"));
			bCheck = new Board(piecesCheck, r, c);
			assertEquals(bCheck, b);
			
			b.move(p, Board.Dir.DOWN); // move down
			piecesCheck = new HashSet<Piece>();
			piecesCheck.add(new Piece("3 2 5 3"));
			bCheck = new Board(piecesCheck, r, c);
			assertEquals(bCheck, b);
			
			b.move(p, Board.Dir.UP); // move up
			piecesCheck = new HashSet<Piece>();
			piecesCheck.add(new Piece("2 2 4 3"));
			bCheck = new Board(piecesCheck, r, c);
			assertEquals(bCheck, b);
		} catch (IllegalMoveException ex){
			System.err.println(ex.getMessage());
			fail();
		}
	}
	
	// test for moving piece along an edge
	@Test
	public void testMoveEdge(){
		Solver.debugOK = true;
		int r = 5, c = 9;
		Piece p = new Piece("1 2 3 3");
		HashSet<Piece> pieces = new HashSet<Piece>();
		HashSet<Piece> piecesCheck;
		pieces.add(p);
		Board b = new Board(pieces, r, c);
		Board bCheck;
		
		try{
			b.move(p, Board.Dir.UP); // move to edge
			piecesCheck = new HashSet<Piece>();
			piecesCheck.add(new Piece("0 2 2 3"));
			bCheck = new Board(piecesCheck, r, c);
			assertEquals(bCheck, b);
			
			try{
				b.move(p, Board.Dir.UP); // invalid move
				fail("Made invalid move");
			} catch (IllegalMoveException ex){
				
			}
			
			b.move(p, Board.Dir.RIGHT); // move along edge
			piecesCheck = new HashSet<Piece>();
			piecesCheck.add(new Piece("0 3 2 4"));
			bCheck = new Board(piecesCheck, r, c);
			assertEquals(bCheck, b);
			
			b.move(p, Board.Dir.LEFT); // move along edge
			piecesCheck = new HashSet<Piece>();
			piecesCheck.add(new Piece("0 2 2 3"));
			bCheck = new Board(piecesCheck, r, c);
			assertEquals(bCheck, b);
			
			b.move(p, Board.Dir.DOWN); // move away from edge
			piecesCheck = new HashSet<Piece>();
			piecesCheck.add(new Piece("1 2 3 3"));
			bCheck = new Board(piecesCheck, r, c);
			assertEquals(bCheck, b);
		} catch (IllegalMoveException ex){
			System.err.println(ex.getMessage());
			fail();
		}
		Solver.debugOK = false;
	}
	
	
	// test for moving piece in corner
	@Test
	public void testMoveCorner(){
		Solver.debugOK = true;
		int r = 6, c = 6;
		Piece p = new Piece("3 4 5 5");
		HashSet<Piece> pieces = new HashSet<Piece>();
		HashSet<Piece> piecesCheck;
		pieces.add(p);
		Board b = new Board(pieces, r, c);
		Board bCheck;
		
		try{
			b.move(p, Board.Dir.UP); // move out of corner
			piecesCheck = new HashSet<Piece>();
			piecesCheck.add(new Piece("2 4 4 5"));
			bCheck = new Board(piecesCheck, r, c);
			assertEquals(bCheck, b);
			
			b.move(p, Board.Dir.DOWN); // move into corner
			piecesCheck = new HashSet<Piece>();
			piecesCheck.add(new Piece("3 4 5 5"));
			bCheck = new Board(piecesCheck, r, c);
			assertEquals(bCheck, b);
			
			b.move(p, Board.Dir.LEFT); // move out of corner
			piecesCheck = new HashSet<Piece>();
			piecesCheck.add(new Piece("3 3 5 4"));
			bCheck = new Board(piecesCheck, r, c);
			assertEquals(bCheck, b);
			
			b.move(p, Board.Dir.RIGHT); // move into corner
			piecesCheck = new HashSet<Piece>();
			piecesCheck.add(new Piece("3 4 5 5"));
			bCheck = new Board(piecesCheck, r, c);
			assertEquals(bCheck, b);
			
			try{
				b.move(p, Board.Dir.RIGHT); // invalid move
				fail("made invalid move");
			} catch (IllegalMoveException ex){
				
			}
			
			try{
				b.move(p, Board.Dir.DOWN); // invalid move
				fail("made invalid move");
			} catch (IllegalMoveException ex){
				
			}
			
		} catch (IllegalMoveException ex){
			System.err.println(ex.getMessage());
			fail();
		}
		Solver.debugOK = false;
	}
	
	// test for moving adjacent pieces
	@Test
	public void testMoveAdjacent(){
		Solver.debugOK = true;
		int r = 6, c = 6;
		Piece p1 = new Piece("3 4 5 5");
		Piece p2 = new Piece("3 3 3 3");
		HashSet<Piece> pieces = new HashSet<Piece>();
		HashSet<Piece> piecesCheck;
		pieces.add(p1);
		pieces.add(p2);
		Board b = new Board(pieces, r, c);
		Board bCheck;
		
		try{
			b.move(p1, Board.Dir.UP); // move large piece
			piecesCheck = new HashSet<Piece>();
			piecesCheck.add(new Piece("2 4 4 5"));
			piecesCheck.add(new Piece("3 3 3 3"));
			bCheck = new Board(piecesCheck, r, c);
			assertEquals(bCheck, b);
			
			b.move(p1, Board.Dir.DOWN); // move large piece
			piecesCheck = new HashSet<Piece>();
			piecesCheck.add(new Piece("3 4 5 5"));
			piecesCheck.add(new Piece("3 3 3 3"));
			bCheck = new Board(piecesCheck, r, c);
			assertEquals(bCheck, b);
			
			try{
				b.move(p1, Board.Dir.LEFT); // blocked by small piece
				fail("made invalid move");
			} catch (IllegalMoveException ex){
			}
			
			b.move(p2, Board.Dir.UP); // move small piece
			piecesCheck = new HashSet<Piece>();
			piecesCheck.add(new Piece("3 4 5 5"));
			piecesCheck.add(new Piece("2 3 2 3"));
			bCheck = new Board(piecesCheck, r, c);
			assertEquals(bCheck, b);
			
			b.move(p2, Board.Dir.DOWN); // move small piece
			piecesCheck = new HashSet<Piece>();
			piecesCheck.add(new Piece("3 4 5 5"));
			piecesCheck.add(new Piece("3 3 3 3"));
			bCheck = new Board(piecesCheck, r, c);
			assertEquals(bCheck, b);
			
			try{
				b.move(p2, Board.Dir.RIGHT); // blocked by large piece
				fail("made invalid move");
			} catch (IllegalMoveException ex){
			}
			
		} catch (IllegalMoveException ex){
			System.err.println(ex.getMessage());
			fail();
		}
		Solver.debugOK = false;
	}
	
	@Test
	public void testGetPossibleMovesCorners(){
		HashSet<Piece> pieces;
		ArrayList<Move> moves, moveCheck;
		Board b;
		
		pieces = new HashSet<Piece>();
		moveCheck = new ArrayList<Move>();
		Piece p1 = new Piece("0 0 0 0");
		Move m1 = new Move(p1, Board.Dir.DOWN);
		Move m2 = new Move(p1, Board.Dir.RIGHT);
		pieces.add(p1);
		moveCheck.add(m1);
		moveCheck.add(m2);
		Piece p2 = new Piece("0 4 1 5");
		Move m3 = new Move(p2, Board.Dir.LEFT);
		Move m4 = new Move(p2, Board.Dir.DOWN);
		pieces.add(p2);
		moveCheck.add(m3);
		moveCheck.add(m4);
		Piece p3 = new Piece("4 4 6 5");
		Move m5 = new Move(p3, Board.Dir.UP);
		Move m6 = new Move(p3, Board.Dir.LEFT);
		pieces.add(p3);
		moveCheck.add(m5);
		moveCheck.add(m6);
		Piece p4 = new Piece("4 0 6 0");
		pieces.add(p4);
		Move m7 = new Move(p4, Board.Dir.UP);
		Move m8 = new Move(p4, Board.Dir.RIGHT);
		moveCheck.add(m7);
		moveCheck.add(m8);
		b = new Board(pieces, 7, 6);
		moves = b.getPossibleMoves();
		for (Move m : moveCheck){
			assertTrue(moves.contains(m));
			moves.remove(m);
		}
		assertTrue(moves.isEmpty());
	}
	
	@Test
	public void testGetPossibleMovesEdges(){
		HashSet<Piece> pieces;
		ArrayList<Move> moves, moveCheck;
		Board b;
		
		pieces = new HashSet<Piece>();
		moveCheck = new ArrayList<Move>();
		Piece p1 = new Piece("0 1 0 1");
		Move m1 = new Move(p1, Board.Dir.DOWN);
		Move m2 = new Move(p1, Board.Dir.RIGHT);
		Move m3 = new Move(p1, Board.Dir.LEFT);
		pieces.add(p1);
		moveCheck.add(m1);
		moveCheck.add(m2);
		moveCheck.add(m3);
		Piece p2 = new Piece("1 3 3 4");
		Move m4 = new Move(p2, Board.Dir.LEFT);
		Move m5 = new Move(p2, Board.Dir.DOWN);
		Move m6 = new Move(p2, Board.Dir.UP);
		pieces.add(p2);
		moveCheck.add(m4);
		moveCheck.add(m5);
		moveCheck.add(m6);
		Piece p3 = new Piece("2 0 4 0");
		Move m7 = new Move(p3, Board.Dir.RIGHT);
		Move m8 = new Move(p3, Board.Dir.UP);
		Move m9 = new Move(p3, Board.Dir.DOWN);
		pieces.add(p3);
		moveCheck.add(m7);
		moveCheck.add(m8);
		moveCheck.add(m9);
		Piece p4 = new Piece("5 2 6 2");
		pieces.add(p4);
		Move m10 = new Move(p4, Board.Dir.LEFT);
		Move m11 = new Move(p4, Board.Dir.UP);
		Move m12 = new Move(p4, Board.Dir.RIGHT);
		moveCheck.add(m10);
		moveCheck.add(m11);
		moveCheck.add(m12);
		b = new Board(pieces, 7, 5);
		moves = b.getPossibleMoves();
		for (Move m : moveCheck){
			assertTrue(moves.contains(m));
			moves.remove(m);
		}
		assertTrue(moves.isEmpty());
	}
	
	@Test
	public void testGetPossibleMovesAdjacentPieces(){
		HashSet<Piece> pieces;
		ArrayList<Move> moves, moveCheck;
		Board b;
		
		pieces = new HashSet<Piece>();
		moveCheck = new ArrayList<Move>();
		
		Piece p1 = new Piece("5 4 5 4");
		Move m1 = new Move(p1, Board.Dir.DOWN);
		Move m2 = new Move(p1, Board.Dir.RIGHT);
		Move m3 = new Move(p1, Board.Dir.LEFT);
		Move m4 = new Move(p1, Board.Dir.UP);
		pieces.add(p1);
		moveCheck.add(m1);
		moveCheck.add(m2);
		moveCheck.add(m3);
		moveCheck.add(m4);
		
		Piece p2 = new Piece("1 1 5 1");
		Move m5 = new Move(p2, Board.Dir.LEFT);
		Move m6 = new Move(p2, Board.Dir.DOWN);
		Move m7 = new Move(p2, Board.Dir.UP);
		pieces.add(p2);
		moveCheck.add(m5);
		moveCheck.add(m6);
		moveCheck.add(m7);
		
		Piece p3 = new Piece("2 2 4 3");
		Move m8 = new Move(p3, Board.Dir.DOWN);
		Move m9 = new Move(p3, Board.Dir.UP);
		pieces.add(p3);
		moveCheck.add(m8);
		moveCheck.add(m9);
		
		Piece p4 = new Piece("1 4 2 4");
		Move m10 = new Move(p4, Board.Dir.RIGHT);
		Move m11 = new Move(p4, Board.Dir.DOWN);
		Move m12 = new Move(p4, Board.Dir.UP);
		pieces.add(p4);
		moveCheck.add(m10);
		moveCheck.add(m11);
		moveCheck.add(m12);
		
		b = new Board(pieces, 7, 6);
		moves = b.getPossibleMoves();
		for (Move m : moveCheck){
			assertTrue(moves.contains(m));
			moves.remove(m);
		}
		assertTrue(moves.isEmpty());
	}
	
	@Test
	public void testGetPossibleMovesBlockedPiece(){
		HashSet<Piece> pieces;
		ArrayList<Move> moves;
		Board b;
		
		pieces = new HashSet<Piece>();
		
		Piece p1 = new Piece("3 2 3 2");
		Piece p2 = new Piece("5 1 5 1");
		Piece p3 = new Piece("4 2 5 3");
		pieces.add(p1);
		pieces.add(p2);
		pieces.add(p3);
		
		b = new Board(pieces, 6, 4);
		moves = b.getPossibleMoves();
		moves.remove(new Move(p1, Board.Dir.UP));
		moves.remove(new Move(p1, Board.Dir.LEFT));
		moves.remove(new Move(p1, Board.Dir.RIGHT));
		moves.remove(new Move(p2, Board.Dir.UP));
		moves.remove(new Move(p2, Board.Dir.LEFT));
		assertTrue(moves.isEmpty());
	}
	
	@Test
	public void testIsOK(){
		ArrayList<Board> boards = new ArrayList<Board>();
		HashSet<Piece> pieces;
		Board b;
		
		pieces = new HashSet<Piece>();
		b = new Board(pieces, 1, 1); //empty 1x1
		boards.add(b);
		
		b = new Board(pieces, 1, 5); //empty with 1 row
		boards.add(b);
		
		b = new Board(pieces, 5, 1); //empty with 1 column
		boards.add(b);
		
		b = new Board(pieces, 5, 5); //empty with multiple rows and columns
		boards.add(b);
		
		pieces = new HashSet<Piece>();
		pieces.add(new Piece("0 0 0 3"));
		b = new Board(pieces, 4, 4); //board with single piece
		boards.add(b);
		
		pieces = new HashSet<Piece>();
		pieces.add(new Piece("5 4 5 4"));
		pieces.add(new Piece("1 1 5 1"));
		pieces.add(new Piece("2 2 4 3"));
		pieces.add(new Piece("1 4 2 4"));
		b = new Board(pieces, 6, 5); //board with multiple pieces
		boards.add(b);
		
		pieces = new HashSet<Piece>();
		pieces.add(new Piece("5 4 5 4"));
		pieces.add(new Piece("1 1 5 1"));
		pieces.add(new Piece("2 2 4 3"));
		pieces.add(new Piece("1 4 2 4"));
		b = new Board(pieces, 6, 5); //board with multiple pieces
		boards.add(b);
		
		for (int i=0; i<boards.size(); i++){
			try {
				boards.get(i).isOK();
			} catch (IllegalStateException ex){
				fail("board "+i+" failed");
			}
		}
	}
	
	@Test
	public void testGetPieces(){
		HashSet<Piece> pieces = new HashSet<Piece>();
		HashSet<Piece> result;
		Piece p1, p2;
		
		//empty board
		Board b = new Board(pieces, 5, 5);
		result = b.getPieces();
		assertTrue(result.isEmpty());
		
		//board with 1 piece
		pieces = new HashSet<Piece>();
		pieces.add(new Piece("2 3 3 3"));
		b = new Board(pieces, 5, 5);
		result = b.getPieces();
		assertEquals(1, result.size());
		assertTrue(result.contains(new Piece("2 3 3 3")));
		
		//board with 2 pieces
		pieces = new HashSet<Piece>();
		pieces.add(new Piece("2 3 3 3"));
		pieces.add(new Piece("0 0 0 0"));
		b = new Board(pieces, 5, 5);
		result = b.getPieces();
		assertEquals(2, result.size());
		assertTrue(result.contains(new Piece("2 3 3 3")));
		assertTrue(result.contains(new Piece("0 0 0 0")));
		
		//moving pieces on the board doesn't change the pieces in the hashset
		try{
			b.move(new Piece("0 0 0 0"), Board.Dir.DOWN);
		} catch (IllegalMoveException ex){
			fail("invalid move made");
		}
		assertEquals(2, result.size());
		assertTrue(result.contains(new Piece("2 3 3 3")));
		assertTrue(result.contains(new Piece("0 0 0 0")));
	}

}
