import java.util.HashSet;
import java.awt.Point;

// This class represents a move that is made on the Board.

public class Move {

	public Piece initPiece;
	public Piece piece;
	public Board.Dir direction;
	public HashSet<Piece> config;
	public Move prev;
	public boolean undo;
	
	// Move constructor. It takes in a Piece and the direction
	// it will be moved in.
	public Move(Piece p, Board.Dir d){
		piece = p;
		direction = d;
	}
	
	public Move(Piece p, Board.Dir d, Piece prev){
		this.piece = p;
		this.direction = d;
		this.initPiece = prev;
	}
	
	public Move(boolean undo, Piece p, Board.Dir d){
		this(p, d);
		this.undo = undo;
	}
	
	public String output(){
		Point pos = piece.getPosition();
		int x = pos.x;
		int y = pos.y;
		String result = x + " " + y;
		switch(direction){
			case UP:
				x--;
				break;
			case DOWN:
				x++;
				break;
			case LEFT:
				y--;
				break;
			case RIGHT:
				y++;
				break;
		}
		return result + " " + x + " " + y;
	}

	// Returns a String representation of the move
	public String toString(){
		return piece + " : " + direction;
	}
	
	// Checks if two Moves are equal
	public boolean equals(Object obj){
		Move m;
		try {
			m = (Move)obj;
		} catch (ClassCastException ex){
			return false;
		}
		return this.piece.equals(m.piece) && this.direction==m.direction;
	}
	
	// Hashcode for Move objects
	public int hashCode(){
		switch(direction){
			case UP:
				return piece.hashCode();
			case DOWN:
				return piece.hashCode()*2;
			case LEFT:
				return piece.hashCode()*3;
			case RIGHT:
				return piece.hashCode()*4;
		}
		return 0;
	}
	
	
	
}
