import static org.junit.Assert.*;

import org.junit.Test;


public class MoveTest {

	@Test
	public void testEquals() {
		Piece p1 = new Piece("2 3 6 6");
		Piece p2 = new Piece("2 3 6 6");
		Piece p3 = new Piece("2 3 3 6");
		Move m1 = new Move(p1, Board.Dir.UP);
		Move m2 = new Move(p2, Board.Dir.UP);
		assertTrue(m1.equals(m2));
		Move m3 = new Move(p3, Board.Dir.UP);
		assertFalse(m1.equals(m3));
		Move m4 = new Move(p1, Board.Dir.DOWN);
		assertFalse(m4.equals(m1));
	}
	
	@Test
	public void testOutput(){
		Piece p1 = new Piece("2 3 3 4");
		Move m1 = new Move(p1, Board.Dir.UP);
		Move m2 = new Move(p1, Board.Dir.DOWN);
		Move m3 = new Move(p1, Board.Dir.LEFT);
		Move m4 = new Move(p1, Board.Dir.RIGHT);
		assertEquals("2 3 1 3", m1.output());
		assertEquals("2 3 3 3", m2.output());
		assertEquals("2 3 2 2", m3.output());
		assertEquals("2 3 2 4", m4.output());
	}

}
