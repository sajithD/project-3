import static org.junit.Assert.*;

import java.awt.Point;

import org.junit.Test;
import java.util.*;


public class PieceTest {
	
	@Test
	public void testPieceConstructor1(){
		Piece p0 = new Piece(new Point(1,1), new Point(1,1));
		Piece p1 = new Piece(new Point(2,10), new Point(3,20));
		assertEquals("1 1 1 1", p0.toString());
		assertEquals("2 10 3 20", p1.toString());
	}

	@Test
	public void testException() {
		boolean output = false;
		try {
			Piece test = new Piece("2 3 4 0 1");
		}
		catch (IllegalArgumentException e) {
			output = true;
			System.out.println(e.getMessage());
		}
		assertTrue(output);

		try{
			Piece test2 = new Piece("0 0 2 1");
		}
		catch (IllegalArgumentException e) {
			output = false;
			System.out.println(e.getMessage());
		}
		assertTrue(output);
		
		try {
			Piece test3 = new Piece(new Point(1,1), new Point(0,0));
		}
		catch (IllegalArgumentException e) {
			output = false;
			System.out.println(e.getMessage());
		}
		assertFalse(output);
		
		try {
			Piece test4 = new Piece("1 1 0 0");
		}
		catch (IllegalArgumentException e) {
			output = true;
			System.out.println(e.getMessage());
		}
		assertTrue(output);
	}
	
	@Test
	public void testPieceConstructor2(){
		Piece p0 = new Piece("0 0 0 0");
		Piece p1 = new Piece(new Point(0,0), new Point(0,0));
		Piece p2 = new Piece("2 10 3 20");
		Piece p3 = new Piece(new Point(2,10), new Point(3,20));
		assertEquals(p0, p1);
		assertEquals(p2, p3);
		
	}

	@Test
	public void hashCodeTest() {
		
	}
	
	@Test
	public void equalsTest(){
		Piece p0 = new Piece("0 0 1 1");
		Piece p1 = new Piece("0 0 1 1");
		assertEquals(p0, p1);
		
		p0 = new Piece("0 10 59 60");
		p1 = new Piece("0 10 59 60");
		assertEquals(p0, p1);
		
		p0 = new Piece("1 9 15 20");
		p1 = new Piece("0 9 15 20");
		assertFalse(p0.equals(p1));

		p0 = new Piece("0 10 15 20");
		p1 = new Piece("0 9 15 20");
		assertFalse(p0.equals(p1));
		
		p0 = new Piece("0 10 14 20");
		p1 = new Piece("0 9 15 20");
		assertFalse(p0.equals(p1));
		
		p0 = new Piece("0 10 15 21");
		p1 = new Piece("0 9 15 20");
		assertFalse(p0.equals(p1));
		
		HashSet<Piece> test = new HashSet<Piece>( );
		p0 = new Piece("1 2 1 3");
		test.add(p0);
		p1 = new Piece("1 2 1 3");
		test.remove(p0);
		for(Piece p : test) {
			System.out.println(p);
		}
		assertFalse(test.contains(p1));
	}

	@Test
	public void toStringTest(){
		Piece p0 = new Piece(new Point(0,0), new Point(1,1));
		assertEquals("0 0 1 1", p0.toString());
		
		Piece p1 = new Piece("49 50 100 75");
		assertEquals("49 50 100 75", p1.toString());
	}
	
    @Test
    public void testConstruct() {
            Piece p0 = new Piece("0 0 0 0");
            Piece p1 = new Piece(p0, Board.Dir.DOWN);
            assertTrue(p1.equals(new Piece("1 0 1 0")));
            Piece p2 = new Piece(p1, Board.Dir.RIGHT);
            assertTrue(p2.equals(new Piece("1 1 1 1")));
            Piece p3 = new Piece(p2, Board.Dir.UP);
            assertTrue(p3.equals(new Piece("0 1 0 1")));
            Piece p4 = new Piece(p3, Board.Dir.LEFT);
            assertTrue(p4.equals(new Piece("0 0 0 0")));
    }
    
    @Test
    public void testPieceCopy() {
    	Piece p0 = new Piece("1 2 3 4");
    	Piece p1 = new Piece(p0);
    	assertTrue(p0.equals(p1));
    	assertNotSame(p0,p1);
    	assertNotSame(p0.getPosition(), p1.getPosition());
    	Piece p2 = new Piece(new Point(1,2), new Point(3,4));
    	assertTrue(p1.equals(p2));
    }
    
    @Test
    public void testSetPosition(){
    	Piece p0 = new Piece("1 2 3 7");
    	p0.setPosition(1, 2);
    	assertEquals("1 2 3 7", p0.toString());
    	p0.setPosition(0, 0);
    	assertEquals("0 0 2 5", p0.toString());
    	p0.setPosition(51, 61);
    	assertEquals("51 61 53 66", p0.toString());
    }
    
    @Test
    public void testGetPosition(){
    	Piece p0 = new Piece("1 2 3 7");
    	Point pt = p0.getPosition();
    	assertEquals(new Point(1,2), pt);
    	p0.setPosition(32, 100);
    	pt = p0.getPosition();
    	assertEquals(new Point(32,100), pt);
    }
    
    @Test
    public void testGetHeightGetWidth(){
    	Piece p0 = new Piece("0 0 0 0");
    	Piece p1 = new Piece("1 82 105 220");
    	assertEquals(1, p0.getHeight());
    	assertEquals(1, p0.getWidth());
    	assertEquals(105, p1.getHeight());
    	assertEquals(139, p1.getWidth());
    	
    	p0.setPosition(34, 55);
    	p1.setPosition(2, 82);
    	assertEquals(1, p0.getHeight());
    	assertEquals(1, p0.getWidth());
    	assertEquals(105, p1.getHeight());
    	assertEquals(139, p1.getWidth());
    }
    
    @Test
    public void testSpaces(){
    	Piece p0 = new Piece("0 0 0 0");
    	ArrayList<Point> pts0 = p0.spaces();
    	assertEquals(1, pts0.size());
    	assertTrue(pts0.contains(new Point(0,0)));
    	
    	Piece p1 = new Piece("1 2 3 3");
    	ArrayList<Point> pts1 = p1.spaces();
    	assertEquals(6, pts1.size());
    	assertTrue(pts1.contains(new Point(1,2)));
    	assertTrue(pts1.contains(new Point(1,3)));
    	assertTrue(pts1.contains(new Point(2,2)));
    	assertTrue(pts1.contains(new Point(2,3)));
    	assertTrue(pts1.contains(new Point(3,2)));
    	assertTrue(pts1.contains(new Point(3,3)));
    }
    
    @Test
    public void testSpacesOnEdge(){
    	Piece p;
    	ArrayList<Point> lt,rt,up,dn;
    	
    	p = new Piece("0 0 0 0"); // piece occupies one space 
    	lt = p.spacesOnEdge(Board.Dir.LEFT);
    	rt = p.spacesOnEdge(Board.Dir.RIGHT);
    	up = p.spacesOnEdge(Board.Dir.UP);
    	dn = p.spacesOnEdge(Board.Dir.DOWN);
    	assertEquals(1, lt.size());
    	assertTrue(lt.contains(new Point(0,0)));
    	assertEquals(1, rt.size());
    	assertTrue(rt.contains(new Point(0,0)));
    	assertEquals(1, up.size());
    	assertTrue(up.contains(new Point(0,0)));
    	assertEquals(1, dn.size());
    	assertTrue(dn.contains(new Point(0,0)));
    	
    	p = new Piece("1 2 3 3"); // piece occupies multiple spaces
    	lt = p.spacesOnEdge(Board.Dir.LEFT);
    	rt = p.spacesOnEdge(Board.Dir.RIGHT);
    	up = p.spacesOnEdge(Board.Dir.UP);
    	dn = p.spacesOnEdge(Board.Dir.DOWN);
    	assertEquals(3, lt.size());
    	assertTrue(lt.contains(new Point(1,2)));
    	assertTrue(lt.contains(new Point(2,2)));
    	assertTrue(lt.contains(new Point(3,2)));
    	assertEquals(3, rt.size());
    	assertTrue(rt.contains(new Point(1,3)));
    	assertTrue(rt.contains(new Point(2,3)));
    	assertTrue(rt.contains(new Point(3,3)));
    	assertEquals(2, up.size());
    	assertTrue(up.contains(new Point(1,2)));
    	assertTrue(up.contains(new Point(1,3)));
    	assertEquals(2, dn.size());
    	assertTrue(dn.contains(new Point(3,2)));
    	assertTrue(dn.contains(new Point(3,3)));
    }
}

