import java.util.*;
import java.awt.Point;
import java.util.HashSet;

public class Board {
	
	private Piece[][] board; // first index is the row, second index is the column
	private HashSet<Piece> pieces;
	private HashSet<Point> emptySpaces;
	private static HashMap<Dir, Dir> oppositeDir;
	private final int rows;
	private final int cols;
	private Move prevMove;
	
	// possible directions to move piece
	// rowT: the amount that the Piece will be moved up or down
	// colT: the amount that the Piece will be moved left or right
	public enum Dir {
		LEFT(0, -1), 
		RIGHT(0, 1), 
		UP(-1, 0), 
		DOWN(1, 0);
	
		private final int rowT;
		private final int colT;
		
		Dir(int rowT, int colT) {
			this.rowT = rowT;
			this.colT = colT;
		}
	}
	
	static {
		oppositeDir = new HashMap<Dir, Dir>();
		oppositeDir.put(Dir.UP, Dir.DOWN);
		oppositeDir.put(Dir.DOWN, Dir.UP);
		oppositeDir.put(Dir.LEFT, Dir.RIGHT);
		oppositeDir.put(Dir.RIGHT, Dir.LEFT);
	}
	
	/*
	 * constructor
	 * pieces: ArrayList of pieces that will be on the board
	 * rows: the number of rows on the board
	 * cols: the number of columns on the board
	 * Sets elements of board to reflect the sizes and positions of pieces
	 * Adds a Point to emptySpaces for every position on the board not
	 * occupied by a piece.
	 * throws IllegalArgumentException if arguments specify invalid board
	 */
	public Board(HashSet<Piece> pieces, int rows, int cols) {
		board = new Piece[rows][cols];
		emptySpaces = new HashSet<Point>();
		this.pieces = pieces;
		this.rows = rows;
		this.cols = cols;
		
		// put a reference to a piece at every position on the board
		// that the piece occupies
		for (Piece block : pieces){
			Point pt = block.getPosition();
			int maxRow = (int)pt.getX( ) + block.getHeight();
			int maxCol = (int)pt.getY( ) + block.getWidth();
			
			for (int i=pt.x; i<maxRow; i++){
				for (int j=pt.y; j<maxCol; j++){
					try {
						if (board[i][j] != null)
							throw new IllegalArgumentException("Overlap on position: "+i+","+j);
					} catch (IndexOutOfBoundsException ex){
						throw new IllegalArgumentException(i+","+j+" invalid position for piece");
					}
					board[i][j] = block;
				}
			}
		}
		
		/*
		 * Search the board for null references. Add each one that
		 * is found to the ArrayList emptySpaces as a Point 
		 * representing the row and column of the null reference.
		 */
		for (int i=0; i<rows; i++){
			for (int j=0; j<cols; j++){
				if (board[i][j] == null){
					emptySpaces.add(new Point(i,j));
				}
			}
		}
	}
	
	/*
	 * returns the Piece at the specified row and column
	 * returns null if the position is empty
	 * throws IllegalArgumentException if the specified
	 * row or column don't exist on the board
	 */
	public int getRows( ) {
		return rows;
	}
	
	public int getCols( ) {
		return cols;
	}

	// Returns the Piece object at the specified row and column
	public Piece getPiece(int row, int column){
		try {
			return board[row][column];
		} catch (IndexOutOfBoundsException ex){
			throw new IllegalArgumentException("row "+row+" column "+column+" does not exist on board");
		}
	}

	// p: Piece to be moved
	// d: direction to move piece
	// rowT: the amount of spaces that Piece will be translated to the left
	// or right, valued at either 0 or 1
	// colT: the amount of spaces that Piece will be translated to up or down,
	// valued at either 0 or 1
	// Moves p in direction d by 1 space. Updates board and p to 
	// reflect new position of p
	// If the move is not valid, throws an IllegalMoveException
	public void move(Piece p, Dir d) throws IllegalMoveException{
		if(Solver.debugOK)
			if(!checkMove(p, d))
				throw new IllegalMoveException("Invalid move:"+p+":"+d);

        int rowT = d.rowT;
        int colT = d.colT;
        int row = (int)p.getPosition( ).getX( );
        int col = (int)p.getPosition( ).getY( );
        ArrayList<Point> edge = p.spacesOnEdge(d);
        ArrayList<Point> oppEdge = new ArrayList<Point>( );
        
        switch(d) {
        case LEFT:
                oppEdge = p.spacesOnEdge(Board.Dir.RIGHT);
                p.setPosition(row, col - 1);
                break;
        case RIGHT:
                oppEdge = p.spacesOnEdge(Board.Dir.LEFT);
                p.setPosition(row, col + 1);
                break;
        case UP:
                oppEdge = p.spacesOnEdge(Board.Dir.DOWN);
                p.setPosition(row - 1, col);
                break;
        case DOWN:
                oppEdge = p.spacesOnEdge(Board.Dir.UP);
                p.setPosition(row + 1, col);
                break;
        }

        for(Point space : edge) {
                board[space.x+rowT][space.y+colT] = p;
                emptySpaces.remove(new Point(space.x+rowT, space.y+colT));

        }

        for(Point space : oppEdge) {
                board[space.x][space.y] = null;
                emptySpaces.add(space);
        }

        prevMove = new Move(new Piece(p), d);

        if(Solver.debugOK)
                isOK();
	}
	
	// Moves a Piece in the opposite direction as that specified in the parameter
	public void undo(Piece p, Dir d) throws IllegalMoveException{
		move(p, oppositeDir.get(d));
	}
	
	// p: Piece to be moved
	// d: direction to move piece
	// rowT: the amount of spaces that Piece will be translated to the left
	// or right, valued at either 0 or 1
	// colT: the amount of spaces that Piece will be translated to up or down,
	// valued at either 0 or 1
	// Returns true if the move is possible, false otherwise
	public boolean checkMove(Piece p, Dir d){
		int rowT = d.rowT;
		int colT = d.colT;
		ArrayList<Point> edge = p.spacesOnEdge(d);
		
		switch(d) {
		case LEFT:
			if((int)p.getPosition( ).getY( ) == 0)
				return false;
			break;
		case RIGHT:
			if((int)p.getPosition( ).getY( ) + (int)p.getWidth( ) == cols)
				return false;
			break;
		case UP:
			if((int)p.getPosition( ).getX( ) == 0) {
				return false;
			}
			break;
		case DOWN:
			if((int)p.getPosition( ).getX( ) + (int)p.getHeight( ) == rows)
				return false;
			break;
		}
		
		for(Point block : edge) {
			if(board[(int)block.getX( ) + rowT][(int)block.getY( ) + colT] != null) {
				return false;
			}
		}
		
		return true;
	}
	
	
	// Checks that the configuration of the board and pieces
	// is consistent. If not, throws IllegalStateException
	public void isOK(){
		// Create HashSet of all possible positions on board
		HashSet<Point> positions = new HashSet<Point>();
		for (int r=0; r<rows; r++){
			for (int c=0; c<cols; c++){
				positions.add(new Point(r,c));
			}
		}
		
		// Check that every position a piece occupies points back to that piece.
		for (Piece p : pieces){
			int r1 = p.getPosition().x;
			int c1 = p.getPosition().y;
			int r2 = r1 + p.getHeight();
			int c2 = c1 + p.getWidth();
			for (int r=r1; r<r2; r++){
				for (int c=c1; c<c2; c++){
					if (board[r][c]!=p){
						throw new IllegalStateException("Position "+r+","+c+" does not point to piece "+p+"\n"+this);
					} else {
						positions.remove(new Point(r,c));
					}
				}
			}
		}
		
		// Check that every position in emptySpaces is null
		for (Point p : emptySpaces){
			int r = p.x;
			int c = p.y;
			if (board[r][c] != null){
				throw new IllegalStateException("Position "+r+","+c+" is not empty"+"\n"+"\n"+emptySpaces+"\n"+this);
			} else {
				positions.remove(new Point(r,c));
			}
		}
		
		// Check that all positions were either pointing to a piece or null
		if (!positions.isEmpty()){
			throw new IllegalStateException("Positions not accounted for: " + positions+"\n"+this);
		}
	}
	
	// overrides Objects.equals
	// return true if two boards are equals (all element of Pieces of board are equals)
	public boolean equals(Object obj){
		Piece[][] b = ((Board) obj).board;
		int l0 = board.length;
		if(l0 == b.length){
			for(int i = 0; i < l0; i++){
				int l1 = board[i].length;
				if(l1 == b[i].length){
					for(int j = 0; j < l1; j++){
						if(b[i][j] == null){
							if(board[i][j] != null)
								return false;
						} else if(!board[i][j].equals(b[i][j])){
							return false;
						}
					}
				}else{
					return false;
				}
			}
			return true;
		}
		return false;
	}
	
	// returns ArrayList of all possible ways to move any piece
	// 1 space in any direction
	public ArrayList<Move> getPossibleMoves(){
		//if (Solver.debugMsg)
		//	System.out.println("getPossibleMoves called");
		HashSet<Move> checked = new HashSet<Move>(); // store Moves that have already been checked
		ArrayList<Move> possibleMoves = new ArrayList<Move>(); // store all valid Moves
		
		for (Point empty: emptySpaces){
			// check if the piece above the empty space can be moved down
			if (empty.x > 0){
				addPossibleMoves(empty.x-1, empty.y, Dir.DOWN, checked, possibleMoves);
			}
			
			// check if the piece to the left can be moved right
			if (empty.y > 0){
				addPossibleMoves(empty.x, empty.y-1, Dir.RIGHT, checked, possibleMoves);
			}
			
			// check if the piece below can be moved up
			if (empty.x < rows-1){
				addPossibleMoves(empty.x+1, empty.y, Dir.UP, checked, possibleMoves);
			}
			
			// check if the piece to the right can be moved left
			if (empty.y < cols-1){
				addPossibleMoves(empty.x, empty.y+1, Dir.LEFT, checked, possibleMoves);
			}
		}
		try{
			possibleMoves.remove(new Move(prevMove.piece, Board.oppositeDir.get(prevMove.direction)));
		}catch(NullPointerException e){
			
		}
		return possibleMoves;
	}
	
	/* A helper method for getPossibleMoves that checks if the Piece
	   in the specified row and column can be moved in the specified
	   direction. If so, it adds the corresponding Move Object to the
	   given ArrayList of possible moves.
	   */
	private void addPossibleMoves(int row, int col, Dir d, HashSet<Move> checked, ArrayList<Move> moves) {
		Piece p = board[row][col];
		if(p == null)
			return;
		Move m = new Move(p, d);
		if(!checked.contains(m)) {
			checked.add(m);
			if(checkMove(m.piece, m.direction))
				moves.add(m);
		}
	}
	
	// overrides Object.hashCode()
	// returns integer for hash code
	public int hashcode(){
		int total = 0;
		for(Piece piece : pieces){
			total += piece.hashCode();
		}
		return total;
	}

	// Returns the list of Pieces on the board
	public HashSet<Piece> getPieces( ) {
		return pieces;
	}

	// Returns a String representation of the Board
	public String toString(){
		HashMap<Piece, Character> output = new HashMap<Piece, Character>();
		Character label = 'A';
		String result = "";

		for (Piece p : pieces){
			output.put(p, label);
			label++;
		}

		for (int r=0; r<cols*2; r++){
			result += "-";
		}
		result += "\n";
		for (int r=0; r<rows; r++){
			result += "|";
			for (int c=0; c<cols; c++){
				Piece p = board[r][c];
				if (p == null)
					result += "  ";
				else
					result += Character.toString(output.get(p))+" ";
			}
			result = result.substring(0,result.length()-1);
			result+="|\n";
		}
		for (int r=0; r<cols*2; r++){
			result += "-";
		}

		return result;
	}

}
