import java.awt.Point;
import java.util.ArrayList;

public class Piece {
	
	private Point position; //coordinates of top left corner of piece
							//position.x is the row, position.y is the column
	private final int height, width;
	
	// constructor
	// p1: coordinates of top left corner of piece
	// p2: coordinates of bottom right corner of piece
	public Piece(Point p1, Point p2){
		position = p1;
		width = p2.y - p1.y + 1;
		height = p2.x - p1.x + 1;
		if (width < 1 || height < 1) {
			throw new IllegalArgumentException ("Invalid block coordinates");
		}
	}
	
	// constructor
	// coords: String representation of coordinates of
	//         top left and bottom right corners
	public Piece(String coords) throws IllegalArgumentException{
		String[] ptCoords = coords.split(" ");
		if (ptCoords.length != 4){
			throw new IllegalArgumentException ("Invalid piece input");
		}
		position = new Point(Integer.parseInt(ptCoords[0]), Integer.parseInt(ptCoords[1]));
		width =  Integer.parseInt(ptCoords[3]) - position.y + 1;
		height = Integer.parseInt(ptCoords[2]) - position.x + 1;
		if (width < 1 || height < 1) {
			throw new IllegalArgumentException ("Invalid block coordinates");
		}
	} 
	
	// assuming the coordinate input is the upper left corner
	public Piece(Piece p) {
		this.height = p.height;
		this.position = new Point();
		this.position.x = p.position.x;
		this.position.y = p.position.y;
		this.width = p.width;
	}
	
	// Constructor that takes in a Piece and a direction that
	// the Piece will be moved in
	public Piece(Piece p, Board.Dir d) {
		int row = (int)p.getPosition( ).getX( );
		int col = (int)p.getPosition( ).getY( );
		this.height = p.height;
		this.width = p.width;
		switch(d) {
		case LEFT:
			setPosition(row, col - 1);
			break;
		case RIGHT:
			setPosition(row, col + 1);
			break;
		case UP:
			setPosition(row - 1, col);
			break;
		case DOWN:
			setPosition(row + 1, col);
			break;
		}
	}

	// changes the position of the piece
	// p: new coordinates for top left corner of piece
	public void setPosition(int x, int y){
		position = new Point(x, y);
	}
	
	// returns position of Piece represented by Point
	public Point getPosition(){
		return new Point(position.x, position.y);
	}
	
	/// returns height of Piece
	public int getHeight(){
		return height;
	}
	
	// returns width of Piece
	public int getWidth(){
		return width;
	}
	
	// returns an ArrayList of Points representing the 1x1 spaces
	// on the board that are covered by Piece
	public ArrayList<Point> spaces( ) {
		ArrayList<Point> squares = new ArrayList<Point>( );
		for(int i = 0; i < height; i++)
			for(int j = 0; j < width; j++) 
				squares.add(new Point((int)position.getX( ) + i, (int)position.getY( ) + j));
		return squares;
	}
	
	// returns an ArrayList of Points representing the 1x1 spaces
	// on the board that are covered by one edge of Piece
	public ArrayList<Point> spacesOnEdge(Board.Dir d) {
		ArrayList<Point> spaces = new ArrayList<Point>( );
		Point pos = position;
		switch(d) {
			case LEFT:
				for(int i = 0; i < height; i++) {
					spaces.add(new Point(pos.x + i, pos.y));
				}
				break;
			case RIGHT:
				for(int i = 0; i < height; i++) {
					spaces.add(new Point(pos.x + i, pos.y + width - 1));
				}
				break;
			case UP:
				for(int i = 0; i < width; i++) {
					spaces.add(new Point(pos.x, pos.y + i));
				}
				break;
			case DOWN:
				for(int i = 0; i < width; i++) {
					spaces.add(new Point(pos.x + height - 1, pos.y + i));
				}
				break;
		}
		return spaces;
	}
	
	// overrides Object.equals(Object)
	// returns true if obj is a Piece and 
	//   both pieces are the same shape and size
	// returns false otherwise
	public boolean equals(Object obj){
		if (obj == null)
			return false;
		Piece o = (Piece) obj;
		return position.x == o.position.x && position.y == o.position.y && height == o.height && width == o.width;
	}
	
	// overrides Object.hashCode()
	// returns integer for hash code
	public int hashCode(){
		return 1000 * position.x + 100 * position.y + 10 * height + width;
	}
	
	// Returns a String representation of the Piece
	public String toString(){
		return position.x + " " + position.y + " " + (position.x + height - 1) + " " + (position.y + width - 1);
	}
}
